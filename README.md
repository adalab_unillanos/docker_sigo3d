# SIGO 3D

## Sistema de Información Geográfica Online & 3D. Campus Barcelona.

## Instalar docker y redireccionar carpeta por defecto de docker.

## Crear la carpeta:
```
mkdir /docker
```

## ingresar a la carpeta:
```
cd /docker
```

## clonar el repositorio:
```
git clone https://gitlab.com/adalab_unillanos/docker_sigo3d.git sigo3d
```

## ingresar usuario y clave:

## ingresar a la carpeta:
```
cd /docker/sigo3d/nginx/cmc3du
```
## Editar el archivo sigo3d.html:
```
nano sigo3d.html
```
## buscar las líneas del token y colocar el generado en https://cesium.com/platform/cesium-ion/:
```
Cesium.Ion.defaultAccessToken = "";
```

## editar el docker-compose y habilitar el puerto para pruebas y deshabilitar el puerto de producción:
```
nano docker-compose.yml
- 80:80
#- 806:80
```
## ejecutar:
```
docker-compose up --build -d
```
## en un navegador web:
```
localhost
```
